package com.magicmirror;

import com.magicmirror.model.DepartureBoard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoUnit;

import static java.time.temporal.ChronoField.*;

@Controller
public class DepartureBoardController {


    private static final DateTimeFormatter VT_TIME_FORMATTER = new DateTimeFormatterBuilder()
            .appendValue(YEAR, 4)
            .appendLiteral('-')
            .appendValue(MONTH_OF_YEAR, 2)
            .appendLiteral('-')
            .appendValue(DAY_OF_MONTH, 2)
            .appendLiteral(' ')
            .appendValue(HOUR_OF_DAY, 2)
            .appendLiteral(':')
            .appendValue(MINUTE_OF_HOUR, 2)
            .toFormatter();

    @Autowired
    private TripService tripService;

    @GetMapping(value = "")
    public ModelAndView departureBoard() {
        DepartureBoard nextDepartures = tripService.getNextDepartures();
        LocalDateTime now = LocalDateTime.now();
        nextDepartures.getDeparture().forEach(departure -> {
            LocalDateTime departureTime = LocalDateTime.parse(nextDepartures.getServerdate() + " " + departure.getTime(), VT_TIME_FORMATTER);
            String minutes = String.valueOf(ChronoUnit.MINUTES.between(now, departureTime));
            departure.setTime(minutes);
        });
        return new ModelAndView("departureboard", "nextDepartures", nextDepartures);
    }
}

package com.magicmirror;


import com.magicmirror.model.DepartureBoard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class TripService {

    private OAuth2RestOperations restTemplate;

    private final Logger log = LoggerFactory.getLogger(TripService.class);

    @Autowired
    public TripService(OAuth2RestOperations restTemplate) {
        this.restTemplate = restTemplate;
    }

    public DepartureBoard getNextDepartures() {
        LocalDateTime now = LocalDateTime.now();
        String date = now.format(DateTimeFormatter.ISO_DATE);
        String time = now.getHour() + "%3A" + now.getMinute();
        ResponseEntity<DepartureBoard> departureBoardResponseEntity = restTemplate.exchange(
                URI.create("https://api.vasttrafik.se/bin/rest.exe/v2/departureBoard?id=9022014012140001&timeSpan=60&maxDeparturesPerLine=10&date=" + date + "&time=" + time + "&useVas=0&useLDTrain=0&useRegTrain=0&useBus=0&useBoat=0&excludeDR=0&direction=9022014003980005"),
                HttpMethod.GET,
                headers(),
                DepartureBoard.class);

        return departureBoardResponseEntity.getBody();
    }

    private static HttpEntity<Void> headers() {
        HttpHeaders headers = new HttpHeaders();
        //headers.add("Authorization", "Basic dXNlcjp0b210ZQ==");
        return new HttpEntity<Void>(null, headers);

    }
}

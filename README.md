# Departureboard #

* Integrates with REST API provided by Västtrafik for fetching departures
* Authentication with OAuth2
* Simple Thymleaf view refreshing every minute to load new results


### Notes on installation. ###

```
#!bash

sudo mkdir -p /var/spring-boot-magic-mirror
sudo useradd -s /usr/sbin/nologin -r -M -d /var/spring-boot-magic-mirror magicmirror
sudo cp /home/pi/spring-boot-magic-mirror-0.0.1-SNAPSHOT.jar /var/spring-boot-magic-mirror/spring-boot-magic-mirror.jar
sudo ln -s /var/spring-boot-magic-mirror/spring-boot-magic-mirror.jar /etc/init.d/spring-boot-magic-mirror
sudo update-rc.d spring-boot-magic-mirror defaults
sudo chown -R magicmirror:magicmirror /var/spring-boot-magic-mirror
sudo service spring-boot-magic-mirror start
```